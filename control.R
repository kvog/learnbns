
# dataFile  <- "~/Dropbox/Link to VORLESUNGEN/BayesianNetworks/2015/survey.txt"
# sep <- " "

dataFile  <- "~/datasets/FloodDamage/Elbe_2002.csv"
sep <- "," 

usedNodes <- 1:29   # optional parameter; defines nodes from the data set that are considered for BN learning
target    <- "rloss"  # optional parameter; defines target node, if any

iss <- 1 # identical sample size for prior distribution;
         # should take a value > 0
# contNodes <- 2  # defines nodes that are discretized in the learning process
                 # a vector with names or indices of the continuous nodes or
                 # an integer value for automatic detecting nodes with more than contNodes states
discrNodes <- c("ws", "kh", "bt", "own")  # alternative to cont Nodes: defines the discrete nodes  
                     # a vector with names or indices of the discrete nodes 
discrMethod <- "equifreq"  # method for discretization: 'equidist', 'equifreq'

if (substring(discrMethod,1,4) == "equi") {
  nInts <- 5  # number of intervals to discretize each continuous variable into 
}
