readData <- function(dataFile, sep = ",") {
#####################################################################
## reads data, selects relevant variables (defined by usedNodes),
## defines continuous and discrete nodes (contNodes, discrNodes)
## and defines dimension of data set (nRecords, nNodes)
##  
## requires: - dataFile: source of data
##           - sep (optional): seperator in data file (default: sep = ",")
## returns:  - data: data set used for BN learning
#####################################################################   
  
  #####################################
  # read data: 
  #####################################
  dataOrg   <- read.delim(dataFile, sep = sep)
  if (dim(dataOrg)[2] == 1) dataOrg   <- read.csv(dataFile, sep = sep)
  
  #####################################
  # check existence of target variable:
  #####################################
  if (exists("target")) {
    if (length(target) > 0) {
      if (length(target) > 1) {
        warning("'target' has more than one elements.\n",
                "Only first element is used.")
        assign("target", target[1], envir = globalenv())
      }
      if (is.numeric(target)) {
        assign("target", names(dataOrg)[target], envir = globalenv())
        hasTarget <- TRUE
      } else if (is.element(target, names(dataOrg))) {
        hasTarget <- TRUE
      } else {
        warning("'target' is not a valid variable's name.\n",
                "Program continuous without target variable.")
        hasTarget <- FALSE
        assign("target", c(), envir = globalenv())  
      }
    } else {
      hasTarget <- FALSE
    }
  } else {
    hasTarget <- FALSE
  }
  assign("hasTarget", hasTarget, envir = globalenv())
  
  #####################################
  # define usedNodes, if not given:
  #####################################
  if (exists("usedNodes")) {
    if (length(usedNodes) > 0) {
      if (is.numeric(usedNodes)) {
        usedNodes <- names(dataOrg)[usedNodes]
      }  
      validNodes <- is.element(usedNodes, names(dataOrg))
      if (sum(validNodes) == 0) {
        warning("'usedNodes' contains no valid variables.\n",
                "Program continuous with all variables in data set.")
        usedNodes <- names(dataOrg)
      }
      if (sum(!validNodes) > 0) {
        warning("'usedNodes' contains invalid variables.\n",
                "Program continuous with valid variables only.")
        usedNodes <- usedNodes[validNodes]
      }
    } else {  # length(usedNodes) == 0
      usedNodes <- names(dataOrg)
    }
  } else {    # exists(usedNodes) == FALSE
    usedNodes <- names(dataOrg)
  }      
  
  #####################################
  # select usedNodes from data set:    
  #####################################
  if (hasTarget) {
    data <- dataOrg[, c(setdiff(usedNodes, target), target)]
  } else {
    data <- dataOrg[, usedNodes]
  }
  
  #####################################
  # define continuous and discrete variables
  #####################################
  assign("nodeNames", names(data), envir = globalenv())
  if (exists("contNodes")) {
    
    ###################################
    # continuous variables are provided in control file
    if (is.numeric(contNodes)) {
      if (length(contNodes) == 1) {
        # contNodes gives max number of states per node
        # nodes with more states are labeled as continuous and will be discretized:
        maxStates <- contNodes
        nStates   <- foreach(i = 1:dim(data)[2], .combine="c") %dopar% {
          length(table(data[, i]))
        }  
        contNodes <- names(data)[nStates > maxStates]
      } else {
        # contNodes contains indices of continuous nodes
        contNodes <- names(dataOrg)[contNodes]
      }
    } else { 
      if (is.character(contNodes)) {
        # contNodes contains names of the continuous variables
        if (sum( !is.element(contNodes, names(dataOrg)) ) > 0) {
          warning("'contNodes' contains variables that are not in the data set.")
        }
      } else {
        stop("'contNodes' must be a single integer, a vector of integers\n",
             "or a vector with the names of the continuous nodes.")
      }
    }
    assign("contNodes", intersect(nodeNames, contNodes), envir = globalenv())
    assign("discrNodes", setdiff(nodeNames, contNodes), envir = globalenv())
    
  } else if (exists("discrNodes")) {
    
    ###################################
    # discrete variables are provided in control file
    if (is.numeric(discrNodes)) {
      # discrNodes contains indices of discrete nodes
      discrNodes <- names(dataOrg)[discrNodes]
    } else {
      if (is.character(discrNodes)) {
        # discrNodes contains names of the discrete variables
        if (sum( !is.element(discrNodes, names(dataOrg)) ) > 0) {
          warning("'discrNodes' contains variables that are not in the data set.")
        }
      } else {
        stop("'discrNodes' must be  a vector of integers\n",
             "or a vector with the names of the continuous nodes.")
      }
    }  
    assign("discrNodes", intersect(nodeNames, discrNodes), envir = globalenv())
    assign("contNodes", setdiff(nodeNames, discrNodes), envir = globalenv())
    
  } else {
    
    ###################################
    # neither contNodes nor discrNodes are provided
    warning("No definition of 'contNodes' or 'discrNodes' is provided.\n",
            "All variables are assumed to be discrete.")
    assign("contNodes", c(), envir = globalenv())
    assign("discrNodes", nodeNames, envir = globalenv())
  }

  
  #####################################
  # dimensions of the data set
  #####################################
  assign("nRecords", dim(data)[1], envir = globalenv())
  assign("nNodes",   dim(data)[2], envir = globalenv())
  
  return(data)
}


defineSplitPoints <- function(contNodes, data) {
#####################################################################
## define possible split points at which continuous variables can be 
## splitted into discrete intervals
## requires: - contNodes: continuous variables that shall be discretized
##           - data: data with observations of contNodes
## returns:  - list with each list element containing the split points 
##             of a continuous variable    
#####################################################################
  splitPoints <- foreach(node = contNodes) %dopar% {
    counts       <- table(data[node], useNA = "no")
    obsValues    <- as.numeric(names(counts))
    nValues      <- length(obsValues)
    nNodeRecords <- sum(counts)
    
    if (nValues <= 100) {
      #################################
      # if less than 100 different values are observed, 
      # a split point is located between each two succeeding values
      splits <- c(obsValues[1], 
        (obsValues[-1] + obsValues[-nValues]) / 2, obsValues[nValues]) 
      
    } else {
      #################################
      # if more than 100 different calues are observed,
      # a tradeoff between the number of possible split points and
      # the number of observations between 2 split points is required
      if (nNodeRecords <= 1000) {
        nRecordsPerSplit <- ceiling(nNodeRecords / 100) 
      } else {
        nRecordsPerSplit <- ceiling(nNodeRecords**(1/3))
      }
      
      # detection of split points starting from the centre 
      # lower half of split points 
      index <- floor(nValues/2)
      splits <- sum(obsValues[c(index, index + 1)]) / 2
      while (index > 0) {
        sumRecords <-  counts[index]
        if (sumRecords >= nRecordsPerSplit) {
          index      <- index - 1
        } else {
          while (sumRecords < nRecordsPerSplit + 1 ){
            index      <- index - 1
            if (index > 0) {
              sumRecords <- sumRecords + counts[index]
            } else {
              sumRecords <- nRecordsPerSplit + 1 
            }
          }
        }
        if (index > 0){
          splits <- c(sum(obsValues[c(index, index + 1)]) / 2, splits)
        } else {
          splits <- c(obsValues[1], splits)
        }
      } # end while (index > 0)
      
      # upper half of split points 
      index <- floor(nValues/2) + 1
      while (index <= nValues) {
        sumRecords <-  counts[index]
        if (sumRecords >= nRecordsPerSplit) {
          index      <- index + 1
        } else {
          while (sumRecords < nRecordsPerSplit + 1 ){
            index      <- index + 1
            if (index <= nValues) {
              sumRecords <- sumRecords + counts[index]
            } else {
              sumRecords <- nRecordsPerSplit + 1 
            }
          }
        }
        if (index <= nValues){
          splits <- c(splits, sum(obsValues[c(index - 1, index)]) / 2)
        } else {
          splits <- c(splits, obsValues[nValues])
        }
      } # end while (index <= nValues)
    } # end nValues > 100
    splits
  } # end %dopar%
  names(splitPoints) <- contNodes
  return(splitPoints)
}